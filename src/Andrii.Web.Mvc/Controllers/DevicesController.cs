﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Web.Models;
using AdysTech.InfluxDB.Client.Net;
using Andrii.Authorization;
using Andrii.Controllers;
using Andrii.Device.Dto;
using Andrii.Devices;
using Andrii.Devices.Dto;
using Andrii.Users.Dto;
using Andrii.Web.Models.Devices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace Andrii.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Devices)]
    public class DevicesController : AndriiControllerBase
    {
        private readonly IDeviceAppService _deviceAppService;
        public DevicesController(IDeviceAppService deviceAppService)
        {
            _deviceAppService = deviceAppService;
        }

        public async Task<ActionResult> Index()
        {
            var devices = (await _deviceAppService.GetAll(new PagedUserResultRequestDto { MaxResultCount = int.MaxValue })).Items;
            var model = new DeviceListViewModel
            {
                Devices = devices
            };
            return View(model);
        }

        public async Task<ActionResult> Statistics(DateTime dateTime)
        {
            var groupedInfluxData = await _deviceAppService.Differences(dateTime);

            var model = new DeviceStatistics
            {
                GroupedInfluxData = groupedInfluxData
            };
            return View(model);
        }

        public async Task<ActionResult> DeviceStatistics(string name)
        {
            //DateTime dateTime = DateTime.Today.AddDays(-1);

            //var groupedInfluxData = await _deviceAppService.Difference(dateTime, name);

            var model = new DeviceStatistics
            {
                Name = name
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Differences([FromBody]DeviceStatistics deviceStatistics)
        {
            var groupedInfluxData = await _deviceAppService.Differences(deviceStatistics.dateTime.Date);
            var model = new DeviceStatistics
            {
                GroupedInfluxData = groupedInfluxData,
                dateTime = deviceStatistics.dateTime
            };
          
            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> Difference([FromBody]DeviceStatistics deviceStatistics)
        {
            var groupedInfluxData = await _deviceAppService.Difference(deviceStatistics.dateTime.Date, deviceStatistics.Name);
            var model = new DeviceStatistics
            {
                GroupedInfluxData = groupedInfluxData,
                dateTime = deviceStatistics.dateTime
            };

            return Json(model);
        }

        public async Task<ActionResult> EditDeviceModal(long deviceId)
        {
            var devices = await _deviceAppService.Get(new EntityDto<long>(deviceId));
            var model = new EditDeviceModalViewModel
            {
                Device = devices
            };
            return View("_EditDeviceModal", model);
        }

        [HttpPost]
        public async Task<IActionResult> Excel(string dateTime)
        {
            DateTime date = DateTime.ParseExact(dateTime, "dd/MM/yyyy", null);

            var comlumHeadrs = new string[]
            {
                "Time",
                "Value"
            };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Statistics");
                using (var cells = worksheet.Cells[1, 1, 1, 2])
                {
                    cells.Style.Font.Bold = true;
                }
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[2, i + 1].Value = comlumHeadrs[i];
                }
                var groupedInfluxData = await _deviceAppService.Differences(date);

                var j = 3;
                worksheet.Cells["A1"].Value = "Date:";
                worksheet.Cells["B1"].Value = dateTime;
                
                foreach (var item in groupedInfluxData)
                {
                    worksheet.Cells["A" + j].Style.Numberformat.Format = "hh:ss:ms";
                    worksheet.Cells["A" + j].Value = item.Time;
                    worksheet.Cells["B" + j].Value = item.Value;

                    j++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Statistics.xlsx");
        }
    }
}
