﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Andrii.Controllers;

namespace Andrii.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : AndriiControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
