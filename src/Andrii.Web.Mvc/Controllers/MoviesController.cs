﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Andrii.Authorization;
using Andrii.Controllers;
using Andrii.Users;
using Andrii.Web.Models.Users;
using Andrii.Users.Dto;
using Andrii.Web.Models;
using Andrii.Movies;
using Microsoft.AspNetCore.Authorization;
using Andrii.Movies.Dto;
using Andrii.Web.Models.Movies;

namespace Andrii.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Movies)]
    public class MoviesController : AndriiControllerBase
    {
        private readonly IMovieAppService _movieAppService;
        public MoviesController(IMovieAppService movieAppService)
        {
            _movieAppService = movieAppService;
        }

        public async Task<ActionResult> Index()
        {
            var movies = ( await _movieAppService.GetAll(new PagedUserResultRequestDto { MaxResultCount = int.MaxValue })).Items;
            var model = new MovieListViewModel
            {
                Movies = movies

            };
            return View(model);
        }

        public async Task<ActionResult> EditMovieModal(long movieId)
        {
            var movies = await _movieAppService.Get(new EntityDto<long>(movieId));
            var model = new EditMovieModalViewModel
            {
                Movie = movies
            };
            return View("_EditMovieModal", model);
        }

    }
}
