﻿namespace Andrii.Web.Startup
{
    public class PageNames
    {
        public const string Home = "Home";
        public const string About = "About";
        public const string Tenants = "Tenants";
        public const string Users = "Users";
        public const string Roles = "Roles";
        public const string Movies = "Movies";
        public const string Devices = "Devices";
        public const string DeviceMeasurements = "DeviceMeasurements";
    }
}
