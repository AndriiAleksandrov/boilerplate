﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Andrii.Web.Views
{
    public abstract class AndriiViewComponent : AbpViewComponent
    {
        protected AndriiViewComponent()
        {
            LocalizationSourceName = AndriiConsts.LocalizationSourceName;
        }
    }
}
