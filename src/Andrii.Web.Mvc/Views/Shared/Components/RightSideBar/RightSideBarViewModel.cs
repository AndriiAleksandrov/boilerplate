﻿using Andrii.Configuration.Ui;

namespace Andrii.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
