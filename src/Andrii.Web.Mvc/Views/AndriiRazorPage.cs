﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace Andrii.Web.Views
{
    public abstract class AndriiRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected AndriiRazorPage()
        {
            LocalizationSourceName = AndriiConsts.LocalizationSourceName;
        }
    }
}
