using System.Collections.Generic;
using Andrii.Roles.Dto;
using Andrii.Users.Dto;

namespace Andrii.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
