﻿using System.Collections.Generic;
using Andrii.Roles.Dto;

namespace Andrii.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}