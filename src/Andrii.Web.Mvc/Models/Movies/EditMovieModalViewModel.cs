using System.Collections.Generic;
using System.Linq;
using Andrii.Movies.Dto;
using Andrii.Roles.Dto;
using Andrii.Users.Dto;

namespace Andrii.Web.Models.Movies
{
    public class EditMovieModalViewModel
    {
        public MovieDto Movie { get; set; }

    }
}
