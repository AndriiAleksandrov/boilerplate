﻿using Andrii.Movies.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Andrii.Web.Models.Movies
{
    public class MovieListViewModel
    {
        public IReadOnlyList<MovieDto> Movies { get; set; }
    }
}
