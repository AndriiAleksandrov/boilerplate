﻿using Andrii.Device.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Andrii.Web.Models.Devices
{
    public class DeviceStatistics
    {
        public IReadOnlyList<GroupedInfluxData> GroupedInfluxData { get; set; }
        public IReadOnlyList<DaySum> DaySum { get; set; }
        public DateTime dateTime;
        public string Name;
    }
}
