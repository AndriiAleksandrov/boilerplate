﻿using Andrii.Devices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Andrii.Web.Models.Devices
{
    public class EditDeviceModalViewModel
    {
        public DeviceDto Device { get; set; }
    }
}
