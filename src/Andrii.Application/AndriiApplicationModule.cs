﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Andrii.Authorization;

namespace Andrii
{
    [DependsOn(
        typeof(AndriiCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class AndriiApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<AndriiAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(AndriiApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
