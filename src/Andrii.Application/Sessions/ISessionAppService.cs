﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Andrii.Sessions.Dto;

namespace Andrii.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
