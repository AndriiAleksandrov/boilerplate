﻿using Abp.Application.Services;
using AdysTech.InfluxDB.Client.Net;
using Andrii.Device.Dto;
using Andrii.Devices.Dto;
using Andrii.Users.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Andrii.Devices
{
    public interface IDeviceAppService : IAsyncCrudAppService<DeviceDto, long, PagedUserResultRequestDto, DeviceDto, DeviceDto>
    {
        Task<InfluxDBClient> Connect();
        Task<List<DeviceDto>> GetAllDevices();
        Task<List<GroupedInfluxData>> Differences(DateTime dateTime);
        Task<List<GroupedInfluxData>> Difference(DateTime dateTime, string name);
    }
}
