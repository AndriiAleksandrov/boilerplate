﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Andrii.Device.Dto
{
    public class InfluxKWHDifference
    {
        public DateTime Time { get; set; }
        public double Value { get; set; }
    }
    public class InfluxDeviceResult
    {
        public string Name { get; set; }
        public List<InfluxKWHDifference> influxKWHDifferences { get; set; }
    }

    public class GroupedInfluxData
    {
        public String TimeString { get; set; }
        public DateTime Time { get; set; }
        public Double Value { get; set; }
    }

    public class DaySum
    {
        public string Name { get; set; }
        public Double Sum { get; set; }
    }
}