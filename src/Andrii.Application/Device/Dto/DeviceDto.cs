﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Andrii.Devices.Dto
{
    [AutoMapFrom(typeof(Device))]
    public class DeviceDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
