﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using AdysTech.InfluxDB.Client.Net;
using Andrii.Authorization;
using Andrii.Device.Dto;
using Andrii.Devices.Dto;
using Andrii.Users.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andrii.Devices
{
    [AbpAuthorize(PermissionNames.Pages_Users)]
    public class DeviceAppService : AsyncCrudAppService<Device, DeviceDto, long, PagedUserResultRequestDto, DeviceDto, DeviceDto>, IDeviceAppService
    {
        private readonly IDeviceAppService _deviceAppService;

        public DeviceAppService(
            IRepository<Device, long> repository
            )
            : base(repository)
        {
        }

        public async Task<InfluxDBClient> Connect()
        {
            InfluxDBClient client = new InfluxDBClient("http://localhost:8086", "reader", "aLmzVO6y1hGCqSrP8Pky");
            return client;
        }
        
        public async Task<List<DeviceDto>> GetAllDevices()
        {
            return ObjectMapper.Map<List<DeviceDto>>(await Repository.GetAllListAsync());
        }

        public async Task<List<GroupedInfluxData>> Differences(DateTime dateTime)
        {
            DateTime dateTimeCompare = new DateTime(DateTime.MinValue.Ticks);
            int value = DateTime.Compare(dateTime, dateTimeCompare);
            if (value == 0)
            {
                var dataNull = new List<GroupedInfluxData> { };
                return dataNull;
            }

            InfluxDBClient client = new InfluxDBClient("http://localhost:8086", "reader", "aLmzVO6y1hGCqSrP8Pky");
            var devicesName = ObjectMapper.Map<List<DeviceDto>>(await Repository.GetAllListAsync());

            string dateTimeString = dateTime.Year + "-" + dateTime.Month.ToString("00") + "-" + dateTime.Day.ToString("00") + "T" + dateTime.Hour.ToString("00") + ":" + dateTime.Minute.ToString("00") + ":" + dateTime.Second.ToString("00") + "Z";
            DateTime dateTimeStart = dateTime.AddHours(24);
            string dateTimeStartString = dateTimeStart.Year + "-" + dateTimeStart.Month.ToString("00") + "-" + dateTimeStart.Day.ToString("00") + "T" + dateTimeStart.Hour.ToString("00") + ":" + dateTimeStart.Minute.ToString("00") + ":" + dateTimeStart.Second.ToString("00") + "Z";

            var deviceResults = new List<InfluxDeviceResult>();
            foreach (var device in devicesName)
            {
                var kwh = await client.QueryMultiSeriesAsync("data", "SELECT difference(min(value)) FROM data.autogen." + device.Name + " WHERE time >= '" + dateTimeString + "' AND time <= '" + dateTimeStartString + "' Group by time(1h) FILL(0) LIMIT 24");
                var results = kwh.FirstOrDefault(x => x.SeriesName == device.Name).Entries.Select(x => new InfluxKWHDifference() { Time = x.Time, Value = Convert.ToDouble(x.Difference) }).ToList();

                foreach (var result in results)
                {
                    var influxKWHDifference = new InfluxKWHDifference() { Time = result.Time, Value = result.Value };
                    var listInfluxKWHDifference = new List<InfluxKWHDifference> { influxKWHDifference };
                    deviceResults.Add(new InfluxDeviceResult() { Name = device.Name, influxKWHDifferences = listInfluxKWHDifference });
                }
            }

            var data = new List<GroupedInfluxData>();
            foreach (var item in deviceResults)
            {
                var influxData = new GroupedInfluxData();
                influxData.Time = item.influxKWHDifferences[0].Time;
                influxData.Value = item.influxKWHDifferences[0].Value;
                if (data.Count != 0)
                {
                    var found = data.Find(x => x.Time == influxData.Time);
                    if (found == null)
                    {
                        data.Add(influxData);
                    }
                    else
                    {
                        data.Find(x => x.Time == influxData.Time).Value += influxData.Value;
                    }

                }
                else
                {
                    data.Add(influxData);
                }
            }

            //suma roznicy kWH dla kazdego urzadzenia
            var daySum = new List<DaySum>();
            foreach (var item in deviceResults)
            {
                var influxDaySum = new DaySum();
                influxDaySum.Name = item.Name;
                influxDaySum.Sum = item.influxKWHDifferences[0].Value;
                if (daySum.Count != 0)
                {
                    var found = daySum.Find(x => x.Name == influxDaySum.Name);
                    if (found == null)
                    {
                        daySum.Add(influxDaySum);
                    }
                    else
                    {
                        daySum.Find(x => x.Name == influxDaySum.Name).Sum += influxDaySum.Sum;
                    }

                }
                else
                {
                    daySum.Add(influxDaySum);
                }
            }

            var datas = new List<GroupedInfluxData>();
            foreach (var item in data)
            {
                datas.Add(new GroupedInfluxData() { Time = item.Time, TimeString = item.Time.ToString("HH"), Value = Math.Round(item.Value, 3, MidpointRounding.AwayFromZero) });
            }

                return datas;
        }

        public async Task<List<GroupedInfluxData>> Difference(DateTime dateTime, string name)
        {
            //DateTime dateTimeCompare = new DateTime(DateTime.MinValue.Ticks);
            //int value = DateTime.Compare(dateTime, dateTimeCompare);
            //if (value == 0)
            //{
            //    var dataNull = new List<GroupedInfluxData> { };
            //    return dataNull;
            //}

            InfluxDBClient client = new InfluxDBClient("http://localhost:8086", "reader", "aLmzVO6y1hGCqSrP8Pky");

            string dateTimeString = dateTime.Year + "-" + dateTime.Month.ToString("00") + "-" + dateTime.Day.ToString("00") + "T" + dateTime.Hour.ToString("00") + ":" + dateTime.Minute.ToString("00") + ":" + dateTime.Second.ToString("00") + "Z";
            DateTime dateTimeStart = dateTime.AddHours(24);
            string dateTimeStartString = dateTimeStart.Year + "-" + dateTimeStart.Month.ToString("00") + "-" + dateTimeStart.Day.ToString("00") + "T" + dateTimeStart.Hour.ToString("00") + ":" + dateTimeStart.Minute.ToString("00") + ":" + dateTimeStart.Second.ToString("00") + "Z";

            var deviceResults = new List<InfluxDeviceResult>();
            var kwh = await client.QueryMultiSeriesAsync("data", "SELECT difference(min(value)) FROM data.autogen." + name + " WHERE time >= '" + dateTimeString + "' AND time <= '" + dateTimeStartString + "' Group by time(1h) FILL(0) LIMIT 24");
            var results = kwh.FirstOrDefault().Entries.Select(x => new InfluxKWHDifference() { Time = x.Time, Value = Convert.ToDouble(x.Difference) }).ToList();

            var datas = new List<GroupedInfluxData>();
            foreach (var item in results)
            {
                datas.Add(new GroupedInfluxData() { Time = item.Time, TimeString = item.Time.ToString("HH"), Value = Math.Round(item.Value, 3, MidpointRounding.AwayFromZero) });
            }

            return datas;
        }
    }
}
