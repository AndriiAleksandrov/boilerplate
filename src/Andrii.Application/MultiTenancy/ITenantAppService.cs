﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Andrii.MultiTenancy.Dto;

namespace Andrii.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

