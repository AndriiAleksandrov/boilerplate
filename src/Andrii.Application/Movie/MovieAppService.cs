﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using Andrii.Authorization;
using Andrii.Authorization.Accounts;
using Andrii.Authorization.Roles;
using Andrii.Authorization.Users;
using Andrii.Movies.Dto;
using Andrii.Roles.Dto;
using Andrii.Users.Dto;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Andrii.Movies
{
    [AbpAuthorize(PermissionNames.Pages_Users)]
    public class MovieAppService : AsyncCrudAppService<Movie, MovieDto, long, PagedUserResultRequestDto, MovieDto, MovieDto>, IMovieAppService
    {

        public MovieAppService(
            IRepository<Movie, long> repository
            )
            : base(repository) {
        }
    }
}

