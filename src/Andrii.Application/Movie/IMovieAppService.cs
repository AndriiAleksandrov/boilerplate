using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Andrii.Movies.Dto;
using Andrii.Roles.Dto;
using Andrii.Users.Dto;

namespace Andrii.Movies
{
    public interface IMovieAppService : IAsyncCrudAppService<MovieDto, long, PagedUserResultRequestDto, MovieDto, MovieDto>
    {

    }

}
