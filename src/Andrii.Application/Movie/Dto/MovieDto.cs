using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Andrii.Authorization.Users;


namespace Andrii.Movies.Dto
{
    [AutoMapFrom(typeof(Movie))]
    public class MovieDto : EntityDto<long>
    {
        public string Title { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public decimal Price { get; set; }

        public string Genre { get; set; }

        public string Rating { get; set; }

        public int Amount { get; set; }

        public DateTime? AvailableDate { get; set; }
    }
}
