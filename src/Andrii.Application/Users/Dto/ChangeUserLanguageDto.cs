using System.ComponentModel.DataAnnotations;

namespace Andrii.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}