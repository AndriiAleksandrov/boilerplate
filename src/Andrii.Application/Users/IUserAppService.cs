using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Andrii.Roles.Dto;
using Andrii.Users.Dto;

namespace Andrii.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
