﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Andrii.Configuration.Dto;

namespace Andrii.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : AndriiAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
