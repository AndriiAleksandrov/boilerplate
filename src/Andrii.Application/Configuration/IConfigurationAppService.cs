﻿using System.Threading.Tasks;
using Andrii.Configuration.Dto;

namespace Andrii.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
