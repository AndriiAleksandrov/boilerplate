﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Andrii.Authorization.Accounts.Dto;

namespace Andrii.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
