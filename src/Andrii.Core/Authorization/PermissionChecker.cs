﻿using Abp.Authorization;
using Andrii.Authorization.Roles;
using Andrii.Authorization.Users;

namespace Andrii.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
