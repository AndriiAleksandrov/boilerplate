﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Andrii.Devices
{
    [Table("Device")]
    public class Device : Entity<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Device() { }
    }
}
