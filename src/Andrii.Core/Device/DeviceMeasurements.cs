﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Andrii.Devices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Andrii.Devices
{
    [Table("DeviceMeasurements")]
    public class DeviceMeasurements : Entity<long>, IHasCreationTime
    {
        [ForeignKey("DeviceId")]
        public Device Device { get; set; }
        public long DeviceId { get; set; }

        public double Voltage { get; set; }
        public double Current { get; set; }
        public double KWH { get; set; }
        public DateTime Time { get; set; }
        public virtual DateTime CreationTime { get; set; }

        public DeviceMeasurements()
        {
            CreationTime = DateTime.Now;
        }
    }
}
