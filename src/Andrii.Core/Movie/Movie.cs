﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using Andrii.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Andrii.Movies
{
    [Table("AppMovie")]

    public class Movie : Entity<long>
    {
        [Required]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        //[ForeignKey("UserID")]
        //public User User { get; set; }
        //public long UserId { get; set; }

        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required]
        [StringLength(30)]
        public string Genre { get; set; }

        [Required]
        public string Rating { get; set; }

        [Display(Name = "Available to lend")]
        [Required]
        public int Amount { get; set; }

        [DataType(DataType.Date)]
        public DateTime? AvailableDate { get; set; }

        public Movie() { }

    }

}
