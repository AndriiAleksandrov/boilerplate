﻿namespace Andrii
{
    public class AndriiConsts
    {
        public const string LocalizationSourceName = "Andrii";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
