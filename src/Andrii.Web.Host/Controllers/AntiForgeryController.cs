using Microsoft.AspNetCore.Antiforgery;
using Andrii.Controllers;

namespace Andrii.Web.Host.Controllers
{
    public class AntiForgeryController : AndriiControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
