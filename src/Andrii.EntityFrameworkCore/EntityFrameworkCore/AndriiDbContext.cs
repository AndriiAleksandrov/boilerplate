﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Andrii.Authorization.Roles;
using Andrii.Authorization.Users;
using Andrii.MultiTenancy;
using Andrii.Movies;
using Andrii.Devices;
using Abp.Localization;

namespace Andrii.EntityFrameworkCore
{
    public class AndriiDbContext : AbpZeroDbContext<Tenant, Role, User, AndriiDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceMeasurements> DeviceMeasurements { get; set; }

        public AndriiDbContext(DbContextOptions<AndriiDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationLanguageText>()
                .Property(p => p.Value)
                .HasMaxLength(100); // any integer that is smaller than 10485760
        }
    }
}
