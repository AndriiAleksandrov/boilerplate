using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Andrii.EntityFrameworkCore
{
    public static class AndriiDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<AndriiDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<AndriiDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }
    }
}
