﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Andrii.Configuration;
using Andrii.Web;

namespace Andrii.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class AndriiDbContextFactory : IDesignTimeDbContextFactory<AndriiDbContext>
    {
        public AndriiDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<AndriiDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            AndriiDbContextConfigurer.Configure(builder, configuration.GetConnectionString(AndriiConsts.ConnectionStringName));

            return new AndriiDbContext(builder.Options);
        }
    }
}
