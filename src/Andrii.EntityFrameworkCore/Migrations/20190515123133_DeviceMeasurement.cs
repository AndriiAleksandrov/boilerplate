﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Andrii.Migrations
{
    public partial class DeviceMeasurement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeviceMeasurements_Device_DeviceID",
                table: "DeviceMeasurements");

            migrationBuilder.DropIndex(
                name: "IX_DeviceMeasurements_DeviceID",
                table: "DeviceMeasurements");

            migrationBuilder.DropColumn(
                name: "DeviceID",
                table: "DeviceMeasurements");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceMeasurements_DeviceId",
                table: "DeviceMeasurements",
                column: "DeviceId");

            migrationBuilder.AddForeignKey(
                name: "FK_DeviceMeasurements_Device_DeviceId",
                table: "DeviceMeasurements",
                column: "DeviceId",
                principalTable: "Device",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeviceMeasurements_Device_DeviceId",
                table: "DeviceMeasurements");

            migrationBuilder.DropIndex(
                name: "IX_DeviceMeasurements_DeviceId",
                table: "DeviceMeasurements");

            migrationBuilder.AddColumn<long>(
                name: "DeviceID",
                table: "DeviceMeasurements",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_DeviceMeasurements_DeviceID",
                table: "DeviceMeasurements",
                column: "DeviceID");

            migrationBuilder.AddForeignKey(
                name: "FK_DeviceMeasurements_Device_DeviceID",
                table: "DeviceMeasurements",
                column: "DeviceID",
                principalTable: "Device",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
