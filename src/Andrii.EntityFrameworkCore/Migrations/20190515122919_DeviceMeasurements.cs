﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Andrii.Migrations
{
    public partial class DeviceMeasurements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeviceMeasurements",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DeviceID = table.Column<long>(nullable: false),
                    DeviceId = table.Column<long>(nullable: false),
                    Voltage = table.Column<double>(nullable: false),
                    Current = table.Column<double>(nullable: false),
                    KWH = table.Column<double>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceMeasurements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceMeasurements_Device_DeviceID",
                        column: x => x.DeviceID,
                        principalTable: "Device",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeviceMeasurements_DeviceID",
                table: "DeviceMeasurements",
                column: "DeviceID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceMeasurements");
        }
    }
}
