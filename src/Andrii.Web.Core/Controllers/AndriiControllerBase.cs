using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Andrii.Controllers
{
    public abstract class AndriiControllerBase: AbpController
    {
        protected AndriiControllerBase()
        {
            LocalizationSourceName = AndriiConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
